package com.mercadolibre.interview.model.builder;

import org.junit.Assert;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class RotationCalculatorTest {

    @Test
    public void getPlanetPositionByDayTest() {

        Point expectedPosition = new Point(0, 1000);

        Assert.assertEquals(expectedPosition,
                RotationCalculator.getPlanetPositionByDay(6, 1000, 15));

        Assert.assertEquals(expectedPosition,
                RotationCalculator.getPlanetPositionByDay(6, 1000, 75));
    }

    @Test
    public void isPointInsideTriangle() {

        List<Point> caseOne = new ArrayList<>();

        caseOne.add(new Point(-1,-1));
        caseOne.add(new Point(0,1));
        caseOne.add(new Point(1,-1));

        Assert.assertTrue(RotationCalculator.isOriginInTriangle(caseOne));

        List<Point> caseTwo = new ArrayList<>();

        caseTwo.add(new Point(0,-1));
        caseTwo.add(new Point(0,1));
        caseTwo.add(new Point(1,-1));

        Assert.assertFalse(RotationCalculator.isOriginInTriangle(caseTwo));
    }

    @Test
    public void arePositionsAlignedTest() {

        List<Point> caseOnePositions = new ArrayList<>();
        caseOnePositions.add(new Point(0, 0));
        caseOnePositions.add(new Point(1, 1));
        caseOnePositions.add(new Point(2, 2));

        Assert.assertTrue(RotationCalculator.arePositionsAligned(caseOnePositions));

        List<Point> caseTwoPositions = new ArrayList<>();
        caseTwoPositions.add(new Point(0, 0));
        caseTwoPositions.add(new Point(1, 1));
        caseTwoPositions.add(new Point(2, 3));

        Assert.assertFalse(RotationCalculator.arePositionsAligned(caseTwoPositions));

        List<Point> caseThreePositions = new ArrayList<>();
        caseThreePositions.add(new Point(0, 0));
        caseThreePositions.add(new Point(1, 0));
        caseThreePositions.add(new Point(2, 0));

        Assert.assertTrue(RotationCalculator.arePositionsAligned(caseThreePositions));

        List<Point> caseFourPositions = new ArrayList<>();
        caseFourPositions.add(new Point(0, 3));
        caseFourPositions.add(new Point(0, 2));
        caseFourPositions.add(new Point(0, 1));

        Assert.assertTrue(RotationCalculator.arePositionsAligned(caseFourPositions));
    }

    @Test
    public void maximumPerimeterTest () {

        List<Point> caseOnePositions = new ArrayList<>();
        caseOnePositions.add(new Point(0, 0));
        caseOnePositions.add(new Point(1, 1));
        caseOnePositions.add(new Point(2, 2));
        //Assert.assertFalse(RotationCalculator.maximumPerimeter(caseOnePositions));
    }
}
