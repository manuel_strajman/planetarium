package utils.parser;

import com.mercadolibre.interview.utils.parser.AngularSpeedParser;
import com.mercadolibre.interview.utils.parser.Parser;
import com.mercadolibre.interview.utils.parser.SunDistanceParser;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SunDistanceParserTest {

    @Test
    public void testSunDistanceParser() {
        Parser parser = new SunDistanceParser();

        String case1 = "3000 km";
        assertTrue(parser.applies(case1));

        assertEquals(3000, parser.parse(case1));


        String case2 = "3000 k";
        assertFalse(parser.applies(case2));
    }
}
