package utils.parser;

import com.mercadolibre.interview.utils.parser.AngularSpeedParser;
import com.mercadolibre.interview.utils.parser.Parser;
import com.mercadolibre.interview.utils.parser.RotationSenseParser;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RotationSenseTest {

    @Test
    public void testRotationSenseParser() {
        Parser parser = new RotationSenseParser();

        String case1 = "clockwise";
        assertTrue(parser.applies(case1));
        assertTrue((boolean) parser.parse(case1));

        String case2 = "counterClockwise";
        assertTrue(parser.applies(case2));
        assertFalse((boolean) parser.parse(case2));

        String case4 = "abc";
        assertFalse(parser.applies(case4));
    }
}
