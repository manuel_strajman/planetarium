package utils.parser;

import com.mercadolibre.interview.utils.parser.AngularSpeedParser;
import com.mercadolibre.interview.utils.parser.Parser;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AngularSpeedParserTest {

    @Test
    public void testAngularSpeedParser() {
        Parser parser = new AngularSpeedParser();

        String case1 = "3000 °/d";
        assertTrue(parser.applies(case1));

        assertEquals(3000, parser.parse(case1));


        String case2 = "3000 °/";
        assertFalse(parser.applies(case2));
    }
}
