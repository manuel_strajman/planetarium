package com.mercadolibre.interview.domain;

public class ClimateConditionsDto {
    private int rainPeriodsAmount;
    private int rainPeakDay;
    private int droughtPeriodsAmount;
    private int optimalConditionsPeriodsAmount;

    public ClimateConditionsDto() {

    }

    public ClimateConditionsDto(int droughtPeriodsAmount, int optimalConditionsPeriodsAmount, int rainPeriodsAmount, int rainPeakDay) {
        this.droughtPeriodsAmount = droughtPeriodsAmount;
        this.optimalConditionsPeriodsAmount = optimalConditionsPeriodsAmount;
        this.rainPeriodsAmount = rainPeriodsAmount;
        this.rainPeakDay = rainPeakDay;
    }

    public int getDroughtPeriodsAmount() {
        return droughtPeriodsAmount;
    }

    public void setDroughtPeriodsAmount(int droughtPeriodsAmount) {
        this.droughtPeriodsAmount = droughtPeriodsAmount;
    }

    public int getOptimalConditionsPeriodsAmount() {
        return optimalConditionsPeriodsAmount;
    }

    public void setOptimalConditionsPeriodsAmount(int optimalConditionsPeriodsAmount) {
        this.optimalConditionsPeriodsAmount = optimalConditionsPeriodsAmount;
    }

    public int getRainPeriodsAmount() {
        return rainPeriodsAmount;
    }

    public void setRainPeriodsAmount(int rainPeriodsAmount) {
        this.rainPeriodsAmount = rainPeriodsAmount;
    }

    public int getRainPeakDay() {
        return rainPeakDay;
    }

    public void setRainPeakDay(int rainPeakDay) {
        this.rainPeakDay = rainPeakDay;
    }
}
