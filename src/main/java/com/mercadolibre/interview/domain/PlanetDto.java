package com.mercadolibre.interview.domain;

public class PlanetDto {

    private String planetName;
    private String angularSpeed;
    private String sunDistance;
    private String rotationSense;

    public PlanetDto() {
    }

    public PlanetDto(String planetName, String angularSpeed, String sunDistance
            , String rotationSense) {
        this.planetName = planetName;
        this.angularSpeed = angularSpeed;
        this.sunDistance = sunDistance;
        this.rotationSense = rotationSense;
    }

    public String getPlanetName() {
        return planetName;
    }

    public void setPlanetName(String planetName) {
        this.planetName = planetName;
    }

    public String getAngularSpeed() {
        return angularSpeed;
    }

    public void setAngularSpeed(String angularSpeed) {
        this.angularSpeed = angularSpeed;
    }

    public String getSunDistance() {
        return sunDistance;
    }

    public void setSunDistance(String sunDistance) {
        this.sunDistance = sunDistance;
    }

    public String getRotationSense() {
        return rotationSense;
    }

    public void setRotationSense(String rotationSense) {
        this.rotationSense = rotationSense;
    }
}
