package com.mercadolibre.interview.domain;

public class DayClimateConditionsDto {
    private String climate;
    private int day;

    public DayClimateConditionsDto(String climate, int day) {
        this.climate = climate;
        this.day = day;
    }

    public String getClimate() {
        return climate;
    }

    public void setClimate(String climate) {
        this.climate = climate;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
