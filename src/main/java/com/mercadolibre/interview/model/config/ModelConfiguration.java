package com.mercadolibre.interview.model.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadolibre.interview.domain.PlanetDto;
import com.mercadolibre.interview.model.data.Planet;
import com.mercadolibre.interview.utils.parser.Parser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan("com.mercadolibre.interview.model")
public class ModelConfiguration {

    private static final String PATTERN = "classpath:planets/*.json";

    private Logger LOGGER = Logger.getLogger(ModelConfiguration.class);

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    public List<PlanetDto> planetDtos() throws IOException {

        List<PlanetDto> planetDtos = new ArrayList<>();
        LOGGER.debug("Reading planet resources from classpath.");

        Resource[] planetResources = (ResourcePatternUtils
                .getResourcePatternResolver(resourceLoader)
                .getResources(PATTERN));

        for(Resource planetResource : planetResources) {
            ObjectMapper mapper = new ObjectMapper();
            planetDtos.add(mapper.readValue(planetResource.getInputStream(), PlanetDto.class));
        }
        return planetDtos;
    }
}
