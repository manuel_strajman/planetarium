package com.mercadolibre.interview.model.repository;

import com.mercadolibre.interview.model.data.ClimateCondition;
import org.springframework.data.repository.CrudRepository;

public interface ClimateConditionRepository extends CrudRepository<ClimateCondition, Integer>{
    int countByClimate(int climate);
}
