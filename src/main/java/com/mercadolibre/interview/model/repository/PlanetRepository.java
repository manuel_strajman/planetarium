package com.mercadolibre.interview.model.repository;

import com.mercadolibre.interview.model.data.Planet;
import org.springframework.data.repository.CrudRepository;

public interface PlanetRepository extends CrudRepository<Planet, Integer> {
}
