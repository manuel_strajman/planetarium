package com.mercadolibre.interview.model.builder;

import com.mercadolibre.interview.model.data.ClimateCondition;
import com.mercadolibre.interview.model.data.Planet;

import java.util.List;

public interface ClimateConditionBuilder {
    List<ClimateCondition> build();
    ClimateConditionBuilder setPlanets(List<Planet> planets);
}
