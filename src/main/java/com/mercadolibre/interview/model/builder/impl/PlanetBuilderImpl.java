package com.mercadolibre.interview.model.builder.impl;

import com.mercadolibre.interview.domain.PlanetDto;
import com.mercadolibre.interview.model.builder.PlanetBuilder;
import com.mercadolibre.interview.model.data.Planet;
import com.mercadolibre.interview.service.ParserService;
import com.mercadolibre.interview.utils.Unit;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PlanetBuilderImpl implements PlanetBuilder {

    private static final Logger LOGGER = Logger.getLogger(PlanetBuilderImpl.class);

    private PlanetDto planetDto;

    @Autowired
    private ParserService parserService;

    public PlanetBuilder setPlanetDto(PlanetDto planetDto) {
        this.planetDto = planetDto;
        return this;
    }

    public Planet build() throws IllegalArgumentException {
        LOGGER.debug("Building planet from planetDto.");
        LOGGER.debug("Setting rotation sense.");
        boolean rotationSense = (boolean) parserService.parseInput(planetDto.getRotationSense());
        LOGGER.debug("Setting angular speed.");
        Integer angularSpeed = (Integer) parserService.parseInput(planetDto.getAngularSpeed());
        LOGGER.debug("Setting sun distance.");
        Integer sunDistance =(Integer) parserService.parseInput(planetDto.getSunDistance());

        LOGGER.debug("Constructing planet.");
        return new Planet(planetDto.getPlanetName(), rotationSense ? angularSpeed : -angularSpeed, sunDistance);
    }
}
