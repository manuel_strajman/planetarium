package com.mercadolibre.interview.model.builder;

import java.awt.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public abstract class RotationCalculator {

    private static final int SCALE = 2;

    public static boolean isOriginInTriangle(List<Point> positions) throws IllegalArgumentException{
        if(positions == null || positions.size() != 3)
            throw new IllegalArgumentException("There must be at least 3 positions to check if origin is inside the triangle.");

        double originx = 0.0;
        double originy = 0.0;

        double p1x = positions.get(0).x;
        double p1y = positions.get(0).y;

        double p2x = positions.get(1).x;
        double p2y = positions.get(1).y;

        double p3x = positions.get(2).x;
        double p3y = positions.get(2).y;

        double alpha = ((p2y - p3y)*(originx - p3x) + (p3x - p2x)*(originy - p3y)) /
                ((p2y - p3y)*(p1x - p3x) + (p3x - p2x)*(p1y - p3y));

        double beta = ((p3y - p1y)*(originx - p3x) + (p1x - p3x)*(originy - p3y)) /
                ((p2y - p3y)*(p1x - p3x) + (p3x - p2x)*(p1y - p3y));

        double gamma = 1 - alpha - beta;

        return alpha > 0 && beta > 0
                && gamma > 0;

    }

    public static Double perimeter(List<Point> positions) {
        Point p1 = positions.get(0);
        Point p2 = positions.get(1);
        Point p3 = positions.get(2);

        return p1.distance(p2) + p1.distance(p3) + p2.distance(p3);
    }

    public static boolean arePositionsAndOriginAligned(List<Point> positions)
        throws IllegalArgumentException {
        List<Point> positionsAndOrigin = new ArrayList<>(positions);
        positionsAndOrigin.add(new Point(0, 0));
        return RotationCalculator.arePositionsAligned(positionsAndOrigin);
    }

    public static boolean arePositionsAligned(List<Point> positions)
            throws IllegalArgumentException {
        if(positions == null || positions.size() < 3) throw new IllegalArgumentException();
        Double slope = null;
        for(int i = 0; i < positions.size() - 1; i++) {
            Point currentPosition = positions.get(i);
            Point nextPosition = positions.get(i + 1);

            Double currentSlope = null;
            if(currentPosition.x != nextPosition.x) {
                currentSlope = ((double) (currentPosition.y - nextPosition.y))
                        / ((double) (currentPosition.x - nextPosition.x));
            }
            if(slope != null && currentSlope != null
                    && !((currentSlope == 0.0 && slope == -0.0)
                            || (currentSlope == -0.0 && slope == 0.0)
                            || currentSlope.doubleValue() == slope.doubleValue())) return false;
            slope = currentSlope;
        }
        return true;
    }

    public static Point getPlanetPositionByDay(Integer angularSpeed,
                                                Integer distance,
                                                int day) throws IllegalArgumentException{
        if(angularSpeed == null)
            throw new IllegalArgumentException("Angular Speed is null.");

        if(distance == null)
            throw new IllegalArgumentException("Distance is null.");

        Integer angle = angularSpeed * day;

        double angleRadians = Math.toRadians(angle.doubleValue());

        Integer x = (new BigDecimal(Math.cos(angleRadians))).multiply(new BigDecimal(distance))
            .setScale(SCALE, RoundingMode.HALF_UP).intValue();
        Integer y =(new BigDecimal(Math.sin(angleRadians))).multiply(new BigDecimal(distance))
            .setScale(SCALE, RoundingMode.HALF_UP).intValue();

        return new Point(x, y);
    }
}
