package com.mercadolibre.interview.model.builder.impl;

import com.mercadolibre.interview.model.builder.ClimateConditionBuilder;
import com.mercadolibre.interview.model.builder.RotationCalculator;
import com.mercadolibre.interview.model.data.ClimateCondition;
import com.mercadolibre.interview.model.data.Planet;
import com.mercadolibre.interview.utils.Climate;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ClimateConditionBuilderImpl implements ClimateConditionBuilder{

    private static final org.apache.log4j.Logger LOGGER = Logger.getLogger(ClimateConditionBuilderImpl.class);

    private static final int AMOUNT_OF_DAYS = 3650;

    private List<Planet> planets;

    public ClimateConditionBuilder setPlanets(List<Planet> planets) {
        this.planets = planets;
        return this;
    }

    @Override
    public List<ClimateCondition> build() {

        List<ClimateCondition> climateConditions = new ArrayList<>();

        ClimateCondition climateCondition = new ClimateCondition();

        climateCondition.setBegin(0);
        LOGGER.info("Simulating planet rotation.");

        Map<Integer, Integer> rainPeakDaysByIndex = new HashMap<>();
        double maximumPerimeter = 0.0;

        for(int day = 0; day < AMOUNT_OF_DAYS; day++) {

            List<Point> positions = new ArrayList<>();
            Climate climate;
            for(Planet planet : planets) {
                positions.add(RotationCalculator.getPlanetPositionByDay(
                        planet.getAngularSpeed(),
                        planet.getDistance(), day));
            }
            if(RotationCalculator.arePositionsAndOriginAligned(positions)) {
                climate = Climate.DROUGHT;
            } else if(RotationCalculator.arePositionsAligned(positions)) {
                climate = Climate.OPTIMAL_PRESSURE;
            } else if (RotationCalculator.isOriginInTriangle(positions)) {

                Double perimeter = RotationCalculator.perimeter(positions);
                if(maximumPerimeter <= perimeter) {
                    if (maximumPerimeter < perimeter) {
                        rainPeakDaysByIndex = new HashMap<>();
                    }
                    rainPeakDaysByIndex.put(climateConditions.size(), day);
                    maximumPerimeter = perimeter;
                }

                climate = Climate.RAIN;
            } else {
                climate = Climate.UNKNOWN;
            }

            if(day > 0 && (climateCondition.getClimate() != climate.getValue()
                    || day == AMOUNT_OF_DAYS - 1)) {
                climateCondition.setEnd(day - 1);
                climateConditions.add(climateCondition);
                climateCondition = new ClimateCondition(day, day, climate.getValue());
            }

        }
        return this.addRainPeakClimateConditions(climateConditions, rainPeakDaysByIndex);
    }

    private List<ClimateCondition> addRainPeakClimateConditions(List<ClimateCondition> climateConditions,
                                              Map<Integer, Integer> rainPeakDaysByIndex) {

        if(rainPeakDaysByIndex.size() > 0) {

            for(Integer index : rainPeakDaysByIndex.keySet()) {
                ClimateCondition rainPeakCondition = climateConditions.get(index);
                climateConditions.remove(index);

                Integer rainPeakDay = rainPeakDaysByIndex.get(index);

                if(rainPeakCondition.getBegin() < rainPeakDay) {
                    climateConditions.add(
                            new ClimateCondition(rainPeakCondition.getBegin(),
                                    rainPeakDay - 1, rainPeakCondition.getClimate()));
                }
                if(rainPeakCondition.getEnd() > rainPeakDay) {
                    climateConditions.add(
                            new ClimateCondition(rainPeakDay + 1,
                                    rainPeakCondition.getEnd(), rainPeakCondition.getClimate()));
                }
                climateConditions.add(new ClimateCondition(rainPeakDay, rainPeakDay, Climate.RAIN_PEAK.getValue()));
            }
        }
        return climateConditions;
    }
}
