package com.mercadolibre.interview.model.builder;

import com.mercadolibre.interview.domain.PlanetDto;
import com.mercadolibre.interview.model.data.Planet;

public interface PlanetBuilder {

    PlanetBuilder setPlanetDto(PlanetDto planetDto);
    Planet build() throws IllegalArgumentException;
}
