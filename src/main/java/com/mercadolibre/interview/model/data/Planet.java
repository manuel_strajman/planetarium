package com.mercadolibre.interview.model.data;

import javax.persistence.*;

@Entity
@Table(name = "planet")
public class Planet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "angular_speed", nullable = false)
    private Integer angularSpeed;

    @Column(name = "distance", nullable = false)
    private Integer distance;

    public Planet() {

    }

    public Planet(String name, Integer angularSpeed, Integer distance) {
        this.name = name;
        this.angularSpeed = angularSpeed;
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAngularSpeed() {
        return angularSpeed;
    }

    public void setAngularSpeed(Integer angularSpeed) {
        this.angularSpeed = angularSpeed;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }
}
