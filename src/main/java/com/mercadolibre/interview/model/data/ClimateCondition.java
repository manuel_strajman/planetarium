package com.mercadolibre.interview.model.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "climate_condition")
public class ClimateCondition {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "climate_id", nullable = false)
    private int id;

    @Column(name = "begin", nullable = false)
    private int begin;

    @Column(name = "last", nullable = false)
    private int end;

    @Column(name = "climate", nullable = false)
    private int climate;

    public ClimateCondition() {
    }

    public ClimateCondition(int begin, int end, int climate) {
        this.begin = begin;
        this.end = end;
        this.climate = climate;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public int getClimate() {
        return climate;
    }

    public void setClimate(int climate) {
        this.climate = climate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
