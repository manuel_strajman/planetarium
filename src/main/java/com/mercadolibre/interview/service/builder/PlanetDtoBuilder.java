package com.mercadolibre.interview.service.builder;

import com.mercadolibre.interview.domain.PlanetDto;
import com.mercadolibre.interview.model.data.Planet;

public interface PlanetDtoBuilder {

    PlanetDtoBuilder setPlanet(Planet planet);
    PlanetDto build();
}
