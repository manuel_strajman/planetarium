package com.mercadolibre.interview.service.builder.impl;

import com.mercadolibre.interview.domain.DayClimateConditionsDto;
import com.mercadolibre.interview.model.data.ClimateCondition;
import com.mercadolibre.interview.service.builder.DayClimateConditionsDtoBuilder;
import com.mercadolibre.interview.utils.Climate;
import org.springframework.stereotype.Component;

@Component
public class DayClimateConditionsDtoBuilderImpl implements DayClimateConditionsDtoBuilder {

    private Iterable<ClimateCondition> climateConditions;

    private Integer day;

    @Override
    public DayClimateConditionsDtoBuilder setClimateConditions(Iterable<ClimateCondition> climateConditions) {
        this.climateConditions = climateConditions;
        return this;
    }

    @Override
    public DayClimateConditionsDtoBuilder setDay(Integer day) {
        this.day = day;
        return this;
    }

    @Override
    public DayClimateConditionsDto build() {
        for(ClimateCondition climateCondition : climateConditions) {
            if(climateCondition.getBegin() <= day
                    && climateCondition.getEnd() >= day) {
                for (Climate climate : Climate.values()) {
                    if (climate.getValue() == climateCondition.getClimate()) {
                        return new DayClimateConditionsDto(climate.toString(), day);
                    }
                }
            }
        }
        return null;
    }
}
