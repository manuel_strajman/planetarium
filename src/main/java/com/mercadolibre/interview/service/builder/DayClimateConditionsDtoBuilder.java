package com.mercadolibre.interview.service.builder;

import com.mercadolibre.interview.domain.DayClimateConditionsDto;
import com.mercadolibre.interview.model.data.ClimateCondition;

import java.util.List;

public interface DayClimateConditionsDtoBuilder {
    DayClimateConditionsDtoBuilder setClimateConditions(Iterable<ClimateCondition> climateConditions);
    DayClimateConditionsDtoBuilder setDay(Integer day);
    DayClimateConditionsDto build();
}
