package com.mercadolibre.interview.service.builder.impl;

import com.mercadolibre.interview.domain.PlanetDto;
import com.mercadolibre.interview.model.data.Planet;
import com.mercadolibre.interview.service.builder.PlanetDtoBuilder;
import com.mercadolibre.interview.utils.Unit;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class PlanetDtoBuilderImpl implements PlanetDtoBuilder {

    private static final Logger LOGGER = Logger.getLogger(PlanetDtoBuilderImpl.class);
    private Planet planet;

    @Override
    public PlanetDtoBuilder setPlanet(Planet planet) {
        this.planet = planet;
        return this;
    }
    @Override
    public PlanetDto build() {

        LOGGER.debug("Building planetDto from planet.");
        LOGGER.debug("Setting planetName");
        String planetName = planet.getName();

        LOGGER.debug("Setting angularSpeed.");
        String angularSpeed = planet.getAngularSpeed() + " " +
                Unit.DEGREES_PER_DAY;

        LOGGER.debug("Setting sun distance.");
        String sunDistance = planet.getDistance() + " " +
                Unit.KILOMETER;

        LOGGER.debug("Setting rotation sense.");
        String rotationSense = planet.getAngularSpeed() >= 0
                ? "clockwise" : "counterClockwise";

        LOGGER.debug("Constructing planetDto.");
        return new PlanetDto(planetName, angularSpeed, sunDistance, rotationSense);
    }
}
