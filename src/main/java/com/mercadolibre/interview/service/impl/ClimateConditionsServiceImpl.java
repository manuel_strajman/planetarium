package com.mercadolibre.interview.service.impl;

import com.mercadolibre.interview.domain.ClimateConditionsDto;
import com.mercadolibre.interview.domain.DayClimateConditionsDto;
import com.mercadolibre.interview.model.data.ClimateCondition;
import com.mercadolibre.interview.model.repository.ClimateConditionRepository;
import com.mercadolibre.interview.service.ClimateConditionsService;
import com.mercadolibre.interview.service.builder.DayClimateConditionsDtoBuilder;
import com.mercadolibre.interview.utils.Climate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClimateConditionsServiceImpl implements ClimateConditionsService {

    private final static Logger logger = Logger.getLogger(ClimateConditionsServiceImpl.class);

    @Autowired
    private ClimateConditionRepository climateConditionRepository;

    @Autowired
    private DayClimateConditionsDtoBuilder dayClimateConditionsDtoBuilder;

    @Override
    public ClimateConditionsDto getClimateConditions() {
        ClimateConditionsDto climateConditionsDto = new ClimateConditionsDto();

        logger.debug("Reading climateConditions from repository and filling dto.");
        climateConditionsDto.setRainPeriodsAmount(
                this.climateConditionRepository
                        .countByClimate(Climate.RAIN.getValue()));

        climateConditionsDto.setRainPeakDay(
                this.climateConditionRepository
                        .countByClimate(Climate.RAIN_PEAK.getValue()));
        climateConditionsDto.setDroughtPeriodsAmount(
                this.climateConditionRepository
                        .countByClimate(Climate.DROUGHT.getValue()));

        climateConditionsDto.setOptimalConditionsPeriodsAmount(
                this.climateConditionRepository
                        .countByClimate(Climate.OPTIMAL_PRESSURE.getValue()));

        return climateConditionsDto;
    }

    @Override
    public DayClimateConditionsDto getDayClimateConditions(Integer day) {
        logger.debug("Reading climate conditions by planet id");
        Iterable<ClimateCondition> climateConditions =
                climateConditionRepository.findAll();
        logger.debug("Searching climate condition in day range.");
        return this.dayClimateConditionsDtoBuilder.setClimateConditions(climateConditions).setDay(day).build();
    }
}
