package com.mercadolibre.interview.service.impl;

import com.mercadolibre.interview.domain.PlanetDto;
import com.mercadolibre.interview.model.data.Planet;
import com.mercadolibre.interview.model.repository.PlanetRepository;
import com.mercadolibre.interview.service.PlanetService;
import com.mercadolibre.interview.service.builder.PlanetDtoBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlanetServiceImpl implements PlanetService {

    private static final Logger LOGGER = Logger.getLogger(PlanetServiceImpl.class);

    @Autowired
    private PlanetRepository planetRepository;


    @Autowired
    private PlanetDtoBuilder planetDtoBuilder;

    @Override
    public List<PlanetDto> getAllPlanets() {
        List<PlanetDto> planetDtos = new ArrayList<>();
        LOGGER.debug("Reading all planets from repository.");
        Iterable<Planet> planets = planetRepository.findAll();
        LOGGER.debug("Initializing dtos from planet entities.");
        planets.forEach(p ->
            planetDtos.add(this.planetDtoBuilder.setPlanet(p).build())
            );
        return planetDtos;
    }
}
