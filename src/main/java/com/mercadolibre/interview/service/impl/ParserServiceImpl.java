package com.mercadolibre.interview.service.impl;

import com.mercadolibre.interview.service.ParserService;
import com.mercadolibre.interview.utils.parser.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParserServiceImpl implements ParserService {

    @Autowired
    private List<Parser> parsers;

    public Object parseInput(String input) throws IllegalArgumentException {
        for(Parser parser : parsers) {
            if(parser.applies(input)) return parser.parse(input);
        }
        throw new IllegalArgumentException();
    }
}
