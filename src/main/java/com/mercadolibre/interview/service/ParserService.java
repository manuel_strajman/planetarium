package com.mercadolibre.interview.service;

public interface ParserService {
    Object parseInput(String input) throws IllegalArgumentException;
}
