package com.mercadolibre.interview.service.config;

import com.mercadolibre.interview.utils.parser.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ComponentScan("com.mercadolibre.interview.service")
public class ServiceConfiguration {

    @Bean
    @Autowired
    public List<Parser> parsers(List<Parser> parsers) {
        return parsers;
    }
}
