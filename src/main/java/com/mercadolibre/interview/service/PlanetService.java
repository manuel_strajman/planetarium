package com.mercadolibre.interview.service;

import com.mercadolibre.interview.domain.PlanetDto;

import java.util.List;

public interface PlanetService {
    List<PlanetDto> getAllPlanets();
}
