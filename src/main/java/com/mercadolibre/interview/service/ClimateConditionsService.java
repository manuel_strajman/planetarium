package com.mercadolibre.interview.service;

import com.mercadolibre.interview.domain.ClimateConditionsDto;
import com.mercadolibre.interview.domain.DayClimateConditionsDto;
import com.mercadolibre.interview.model.data.ClimateCondition;

import java.util.List;

public interface ClimateConditionsService {
    ClimateConditionsDto getClimateConditions();
    DayClimateConditionsDto getDayClimateConditions(Integer day);
}
