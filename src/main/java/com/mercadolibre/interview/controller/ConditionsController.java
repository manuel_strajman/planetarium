package com.mercadolibre.interview.controller;

import com.mercadolibre.interview.domain.ClimateConditionsDto;
import com.mercadolibre.interview.domain.DayClimateConditionsDto;
import com.mercadolibre.interview.service.ClimateConditionsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConditionsController {

    private static final Logger LOGGER = Logger.getLogger(ConditionsController.class);

    @Autowired
    private ClimateConditionsService climateConditionsService;

    @GetMapping("/climate/{day}")
    @ResponseBody
    public DayClimateConditionsDto getConditionsByDayResponse(
            @PathVariable("day") Integer day) throws Exception {

        LOGGER.info("Executing get day climate conditions request.");
        return this.climateConditionsService.getDayClimateConditions(day);
    }

    @GetMapping("/climate")
    @ResponseBody
    public ClimateConditionsDto getConditionsResponse() throws Exception {

        LOGGER.info("Executing get day climate conditions request.");
        return this.climateConditionsService.getClimateConditions();
    }

}
