package com.mercadolibre.interview.controller;

import com.mercadolibre.interview.domain.PlanetDto;
import com.mercadolibre.interview.service.PlanetService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PlanetController {

    private static final Logger LOGGER = Logger.getLogger(PlanetController.class);

    @Autowired
    private PlanetService planetService;

    @GetMapping(path="/planets")
    @ResponseBody
    public List<PlanetDto> getAllPlanetsResponse() throws Exception {
        LOGGER.info("Executing get all Planets request.");
        List<PlanetDto> planetDtos = this.planetService.getAllPlanets();
        return planetDtos;
    }
}
