package com.mercadolibre.interview.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class IndexController {

    @GetMapping({"/"})
    @ResponseBody
    public String indexResponse() {
        return "<html><head><h1>Planetarium API</h1></head><body>" +
                "<h2>Planet Service</h2>" +
                "<h3>(GET) planets</h3>" +
                "<ul><li>/planetarium/planets/</li></ul>" +
                "<h2>Climate Service</h2>" +
                "<h3>(GET) climate conditions</h3>" +
                "<ul><li>/planetarium/climate</li></ul>" +
                "<h3>(GET) day climate conditions</h3>" +
                "<ul><li>/planetarium/climate/{day}</li></ul>" +
                "</body></html>";
    }
}
