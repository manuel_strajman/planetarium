package com.mercadolibre.interview.utils.parser;

public interface Parser<T> {
    boolean applies(String input);
    T parse(String input);
}
