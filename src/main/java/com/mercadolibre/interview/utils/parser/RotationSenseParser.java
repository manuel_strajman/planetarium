package com.mercadolibre.interview.utils.parser;

import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class RotationSenseParser implements Parser<Boolean> {

    private final static Pattern PATTERN = Pattern.compile("-?(counterClockwise)||(clockwise)");

    public boolean applies(String input) {
        return PATTERN.matcher(input).matches();
    }

    public Boolean parse(String input) {
        return input.equals("clockwise");
    }
}
