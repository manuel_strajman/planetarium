package com.mercadolibre.interview.utils.parser;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.regex.Pattern;

@Component
public class SunDistanceParser implements Parser<Integer> {

    private final static Pattern PATTERN = Pattern.compile("\\d+?\\skm$");

    public boolean applies(String input) {
        return PATTERN.matcher(input).matches();
    }

    public Integer parse(String input) {
        String[] splitInput = input.split(" ");
        return Integer.parseInt(splitInput[0]);
    }
}
