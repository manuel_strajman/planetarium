package com.mercadolibre.interview.utils;

public enum Climate {
    RAIN("rain", 0), RAIN_PEAK("rainPeak", 1),
    DROUGHT("drought", 2), OPTIMAL_PRESSURE("optimalPressure", 3),
    UNKNOWN( "unknown", 4);

    private String name;
    private int value;

    Climate(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }
}
