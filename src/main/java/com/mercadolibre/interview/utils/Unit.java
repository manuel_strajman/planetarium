package com.mercadolibre.interview.utils;

public enum Unit {

    DEGREES_PER_DAY("°/d"),
    KILOMETER("km");

    private String name;

    Unit(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
