package com.mercadolibre.interview;

import com.mercadolibre.interview.domain.PlanetDto;
import com.mercadolibre.interview.model.builder.ClimateConditionBuilder;
import com.mercadolibre.interview.model.builder.PlanetBuilder;
import com.mercadolibre.interview.model.data.ClimateCondition;
import com.mercadolibre.interview.model.data.Planet;
import com.mercadolibre.interview.model.repository.ClimateConditionRepository;
import com.mercadolibre.interview.model.repository.PlanetRepository;
import com.mercadolibre.interview.service.ClimateConditionsService;
import com.mercadolibre.interview.service.PlanetService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableAsync
public class App implements CommandLineRunner {
    private static final Logger LOGGER = Logger.getLogger(App.class);

    @Autowired
    private List<PlanetDto> planetDtos;

    @Autowired
    private PlanetBuilder planetBuilder;

    @Autowired
    private ClimateConditionBuilder climateConditionBuilder;

    @Autowired
    private PlanetRepository planetRepository;

    @Autowired
    private ClimateConditionRepository climateConditionRepository;

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Transactional
    public void run(String... String) throws Exception{

        LOGGER.info("Building planets.");
        List<Planet> planets = new ArrayList<>();
        this.planetDtos.forEach(p ->
                planets.add(this.planetBuilder.setPlanetDto(p).build()));

        LOGGER.info("Building climate conditions.");
        List<ClimateCondition> climateConditions =
                this.climateConditionBuilder
                        .setPlanets(planets).build();

        LOGGER.info("Saving Planets into repository.");
        this.planetRepository.save(planets);

        LOGGER.info("Saving ClimateConditions into repository.");
        this.climateConditionRepository.save(climateConditions);
    }
}

